# encoding:utf-8
#!/usr/bin/python

import copy
import csv
import codecs
import cStringIO
import pprint
import getpass
import itertools
import operator
import optparse
import sys
import atom
import gdata.data
import gdata.contacts.client
import gdata.contacts.data
from gdata.gauth import TwoLeggedOAuthHmacToken


# Domain name
CONSUMER_KEY = "tvn.cl"

# oauth key
CONSUMER_SECRET = "LSSh0DV6e0GfYjs0oKndLUV-"


class OAuthManager(object):

    ''' Maneja la autenticación mediante la Key de la Consola del Dominio de Google Apps
    '''

    def __init__(self, user, consumer_key, consumer_secret):
        self.user = user
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.requestor_id = self.user + "@" + self.consumer_key
        self.two_legged_oauth_token = TwoLeggedOAuthHmacToken(
            self.consumer_key, self.consumer_secret, self.requestor_id)


class ContactsManager(object):

    def __init__(self, user, domain, two_legged_oauth_token):
        self.user = user
        self.domain = domain
        self.contact_list = self.user + '@' + self.domain
        self.two_legged_oauth_token = two_legged_oauth_token
        self.contacts_client = gdata.contacts.client.ContactsClient(domain=self.domain)
        self.contacts_client.auth_token = self.two_legged_oauth_token
        #self.feed_uri = self.contacts_client.GetFeedUri(contact_list=self.contact_list, projection='full')
        self.feed_uri = self.GetMyContactsGroupUri()
        self.feed = self.contacts_client.GetFeed(uri=self.feed_uri, desired_class=gdata.contacts.data.ContactsFeed)


    def GetAllContacts(self):


if __name__ == '__main__':
	main()