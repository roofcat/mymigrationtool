# encoding:utf-8
#!/usr/bin/env python

import sys
import getopt
import re
import urllib
import string
import time
import optparse
import encodings
try:
  from xml.etree import ElementTree # for Python 2.5 users
except ImportError:
  from elementtree import ElementTree
import gdata.calendar.client
import gdata.calendar.data
import atom

import icalendar # http://codespeak.net/icalendar/
from gdata.gauth import TwoLeggedOAuthHmacToken


# dominio de google apps
CONSUMER_KEY = "gapps.azurian.com"

# clave oauth de la consola de administracion
CONSUMER_SECRET = "Z_HeTTZfDc04YDrmC_2Ehynu"


# Oauth Manager class
class OAuthManager:

    ''' Maneja la autenticación mediante la Key de la Consola del Dominio de Google Apps
    '''

    def __init__(self, user, consumer_key, consumer_secret):
        self.user = user
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.requestor_id = self.user + "@" + self.consumer_key
        self.two_legged_oauth_token = TwoLeggedOAuthHmacToken(
            self.consumer_key, self.consumer_secret, self.requestor_id)


# Outlook puts a space where it shouldn't in 'third tuesday of the month'
# lines (eg, '3 TU' rather than '3TU')
WEEKDAY_RULE = re.compile('(?P<signal>[+-]?)(?P<relative>[\d]?)'
                          ' ?(?P<weekday>[\w]{2})$')

icalendar.prop.WEEKDAY_RULE = WEEKDAY_RULE


GOOGLE_STRFTIME = '%Y-%m-%dT%H:%M:%S.000Z'


def cals(client):
    feed = client.GetAllCalendarsFeed()
    return feed.entry[0].id.text

class Event(object):
    def __init__(self):
        self.uid = None
        self.start = None
        self.end = None
        self.subject = None
        self.description = None
        self.location = None
        self.recurrence = None

    def __repr__(self):
        s = "[Event"
        for attr in ('uid', 'start', 'end', 'subject', 'description', 'location', 'recurrence'):
            s += (' ' + attr + '=' + repr(getattr(self, attr)))
        s += "]"
        return s

def make_events(s):
    """Extract events from an .ics file's text."""
    cal = icalendar.Calendar.from_ical(s)
    events = []
    for subcomponent in cal.subcomponents:
        if isinstance(subcomponent, icalendar.Timezone):
            pass # TODO - handle this?
        elif isinstance(subcomponent, icalendar.Event):
            event = Event()
            event.uid = subcomponent.decoded('uid')
            # TODO - handle timezone of dtstart TZID param
            event.start = subcomponent.decoded('dtstart')
            event.end = subcomponent.decoded('dtend') # TODO - handle 'duration'
            event.subject = subcomponent.decoded('summary')
            try:
                event.description = unicode(subcomponent.decoded('description'))
            except:
                pass
            try:
                event.location = unicode(subcomponent.decoded('location'))
            except:
                pass
            try:
                if 'rrule' in subcomponent:
                    # Fix Outlook's trigger-happy spacebar finger:
                    for k in subcomponent['rrule']:
                        for i in range(len(subcomponent['rrule'][k])):
                            if isinstance(subcomponent['rrule'][k][i], basestring):
                                subcomponent['rrule'][k][i] = subcomponent['rrule'][k][i].replace(' ', '')
                    event.recurrence = (
                        'DTSTART:' + subcomponent['dtstart'].ical() + '\r\n' +
                        'DTEND:' + subcomponent['dtend'].ical() + '\r\n' +
                        'RRULE:' + subcomponent['rrule'].ical())
            except:
                pass
            events.append(event)
    return events

def send_events(cal_client, events):
    """Send events to Google Calendar.
    
    client - a CalendarService object
    api_url - the URL to send events to
    events - a list of Event objects
    """
    api_url = cals(cal_client)

    for event in events:
        # TODO - we need to check whether to insert or update an existing entry
        gevent = gdata.calendar.data.CalendarEventEntry()
        gevent.title = atom.data.Title(text=event.subject)
        gevent.content = atom.data.Content(text=event.description)
        gevent.where.append(gdata.data.Where(value=event.location))
        if event.recurrence is not None:
            gevent.recurrence = gdata.data.Recurrence(text=event.recurrence)
        gevent.when.append(gdata.data.When(
                            start=event.start.strftime(GOOGLE_STRFTIME),
                            end=event.end.strftime(GOOGLE_STRFTIME)))
        print "sending new event (%s%s) to %s" % (
            event.start.strftime('%Y-%m-%d %H:%M'),
            event.recurrence and ' recurring' or '',
            api_url)
        #print unicode(gevent)
        new_event = cal_client.InsertEvent(gevent)

def make_client(domain, token):
    """Return a CalendarService set up with this email and password"""
    cal_client = gdata.calendar.client.CalendarClient()
    cal_client.domain = domain
    cal_client.auth_token = token
    return cal_client

def main():
    usage = '''\
    migration_calendar.py --user=USER_NAME --file=ICS_FILE
    '''

    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-u', '--user', help='nombre de usuario a migrar')
    parser.add_option('-f', '--file', help='archivo -ics')
    options, args = parser.parse_args()

    if args:
        parser.print_help()
        parser.exit(msg='\nUnexpected arguments: %s' % ' '.join(args))

    user_name = str(options.user)
    ics_file = options.file

    oauth_manager = OAuthManager(user_name, CONSUMER_KEY, CONSUMER_SECRET)
    client = make_client(CONSUMER_KEY, oauth_manager.two_legged_oauth_token)
    
    ical_file = open(ics_file, 'rb')
    events = make_events(ical_file.read())
    send_events(client, events)

if __name__ == '__main__':
    main()
