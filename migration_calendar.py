# encoding:utf-8
#!/usr/bin/env python


import optparse
import re
import vobject
import sys
import os
import string
import time
import shutil
import md5
import atom
from httplib2 import Http

from gdata import service
from gdata import calendar
from gdata.calendar import service
from gdata.calendar import client
from gdata.gauth import TwoLeggedOAuthHmacToken


# dominio de google apps
CONSUMER_KEY = "gapps.azurian.com"

# clave oauth de la consola de administracion
CONSUMER_SECRET = "Z_HeTTZfDc04YDrmC_2Ehynu"


# Oauth Manager class
class OAuthManager(object):

    ''' Maneja la autenticación mediante la Key de la Consola del Dominio de Google Apps
    '''

    def __init__(self, user, consumer_key, consumer_secret):
        self.user = user
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.requestor_id = self.user + "@" + self.consumer_key
        self.two_legged_oauth_token = TwoLeggedOAuthHmacToken(
            self.consumer_key, self.consumer_secret, self.requestor_id)


# Google Calendar class.
class CalendarManager(object):

    def __init__(self, user, domain, two_legged_oauth_token):
        self.user = user
        self.domain = domain
        self.login = self.user + '@' + self.domain
        self.private_url = 'https://www.google.com/calendar/ical/' + \
            self.login + '/private/basic.ics'
        self.two_legged_oauth_token = two_legged_oauth_token
        self.calendar_service = client.CalendarClient()
        self.calendar_service.domain = domain
        self.calendar_service.auth_token = self.two_legged_oauth_token

    # Properly encode unicode characters.
    def encode_element(self, el):
        return unicode(el).encode('ascii', 'replace')

    # Use the Google-compliant datetime format for single events.
    def format_datetime(self, date):
        try:
            if re.match(r'^\d{4}-\d{2}-\d{2}$', str(date)):
                return str(date)
            else:
                return str(time.strftime("%Y-%m-%dT%H:%M:%S.000Z", date.utctimetuple()))
        except Exception, e:
            print type(e), e.args, e
            return str(date)

    # Use the Google-compliant datetime format for recurring events.
    def format_datetime_recurring(self, date):
        try:
            if re.match(r'^\d{4}-\d{2}-\d{2}$', str(date)):
                return str(date).replace('-', '')
            else:
                return str(time.strftime("%Y%m%dT%H%M%SZ", date.utctimetuple()))
        except Exception, e:
            print type(e), e.args, e
            return str(date)

    # Use the Google-compliant alarm format.
    def format_alarm(self, alarm):
        google_minutes = [
            5, 10, 15, 20, 25, 30, 45, 60, 120, 180, 1440, 2880, 10080]
        m = re.match('-(\d+)( day[s]?, )?(\d+):(\d{2}):(\d{2})', alarm)
        try:
            time = m.groups()
            t = 60 * \
                ((int(time[0]) - 1) * 24 + (23 - int(time[2]))) + \
                (60 - int(time[3]))
            # Find the closest minutes value valid for Google.
            closest_min = google_minutes[0]
            closest_diff = sys.maxint
            for m in google_minutes:
                diff = abs(t - m)
                if diff == 0:
                    return m
                if diff < closest_diff:
                    closest_min = m
                    closest_diff = diff
            return closest_min
        except:
            return 0

    # Convert a iCal event to a Google Calendar event.
    def ical2gcal(self, e, dt):
        # Parse iCal event.
        event = {}
        event['uid'] = self.encode_element(dt.uid.value)
        event['subject'] = self.encode_element(dt.summary.value)
        if hasattr(dt, 'description') and (dt.description is not None):
            event['description'] = self.encode_element(
                dt.description.value)
        else:
            event['description'] = ''
        if hasattr(dt, 'location'):
            event['where'] = self.encode_element(dt.location.value)
        else:
            event['where'] = ''
        if hasattr(dt, 'status'):
            event['status'] = self.encode_element(dt.status.value)
        else:
            event['status'] = 'CONFIRMED'
        if hasattr(dt, 'organizer'):
        	try:
        		event['organizer'] = self.encode_element(dt.organizer.params['CN'][0])
    		except:
    			event['organizer'] = ''
        	event['mailto'] = self.encode_element(dt.organizer.value).split('mailto:')
        	print event['mailto']
        	event['mailto'] = re.search('(?<=MAILTO:).+', str(event['mailto']))
        if hasattr(dt, 'rrule'):
            event['rrule'] = self.encode_element(dt.rrule.value)
        if hasattr(dt, 'dtstart'):
            event['start'] = dt.dtstart.value
        if hasattr(dt, 'dtend'):
            event['end'] = dt.dtend.value
        if hasattr(dt, 'valarm'):
            event['alarm'] = self.format_alarm(
                self.encode_element(dt.valarm.trigger.value))

        # Convert into a Google Calendar event.
        try:
            e.title = atom.Title(text=event['subject'])
            e.extended_property.append(
                calendar.ExtendedProperty(name='local_uid', value=event['uid']))
            e.content = atom.Content(text=event['description'])
            e.where.append(
                calendar.Where(value_string=event['where']))
            e.event_status = calendar.EventStatus()
            e.event_status.value = event['status']
            if event.has_key('organizer'):
                attendee = calendar.Who()
                attendee.rel = 'ORGANIZER'
                attendee.name = event['organizer']
                attendee.email = event['mailto']
                attendee.attendee_status = calendar.AttendeeStatus()
                attendee.attendee_status.value = 'ACCEPTED'
                if len(e.who) > 0:
                    e.who[0] = attendee
                else:
                    e.who.append(attendee)
            # TODO: handle list of attendees.
            if event.has_key('rrule'):
                # Recurring event.
                recurrence_data = ('DTSTART;VALUE=DATE:%s\r\n'
                                   + 'DTEND;VALUE=DATE:%s\r\n'
                                   + 'RRULE:%s\r\n') % (
                                       self.format_datetime_recurring(
                                           event['start']),
                                       self.format_datetime_recurring(
                                           event['end']),
                                       event['rrule'])
                e.recurrence = calendar.Recurrence(
                    text=recurrence_data)
            else:
                # Single-occurrence event.
                if len(e.when) > 0:
                    e.when[0] = calendar.When(
                        start_time=self.format_datetime(event['start']),
                        end_time=self.format_datetime(event['end']))
                else:
                    e.when.append(
                        calendar.When(
                            start_time=self.format_datetime(
                                event['start']),
                            end_time=self.format_datetime(event['end'])))
                if event.has_key('alarm'):
                    # Set reminder.
                    for a_when in e.when:
                        if len(a_when.reminder) > 0:
                            a_when.reminder[0].minutes = event['alarm']
                        else:
                            a_when.reminder.append(
                                gdata.calendar.Reminder(minutes=event['alarm']))
        except Exception, e:
            print >> sys.stderr, 'ERROR: couldn\'t create gdata event object: ', event[
                'subject']
            print type(e), e.args, e
            sys.exit(1)

    # Return the list of events in the Google Calendar.
    def elements(self):
        try:
            feed = self.calendar_service.GetCalendarEventFeed()
        except:
            print >> sys.stderr, 'ERROR: couldn\'t retrieve Google Calendar event list'
            sys.exit(1)
        ret = []
        for i, event in enumerate(feed.entry):
            ret.append(event)
        return ret

    # Fix all the Google Calendar events adding the extended property
    # "local_uid" used to properly the single events.
    def fix_remote_uids(self):
        for e in self.elements():
            found = False
            if e.extended_property:
                for num, p in enumerate(e.extended_property):
                    if (p.name == 'local_uid'):
                        found = True
            if not found:
                id = os.path.basename(e.id.text) + '@google.com'
                print 'fixing', id, 'for event', e.id.text
                e.extended_property.append(
                    calendar.ExtendedProperty(name='local_uid', value=id))
                try:
                    new_event = self.calendar_service.UpdateEvent(
                        e.GetEditLink().href, e)
                    print 'Fixed event (%s): %s' % (self.private_url, new_event.id.text,)
                except:
                    print >> sys.stderr, 'WARNING: couldn\'t update entry %s to %s!' % (
                        id, self.private_url)

    # Translate a remote uid into the local uid.
    def get_local_uid(self, uid):
        for e in self.elements():
            local_uid = os.path.basename(e.id.text)
            if not re.match(r'@google\.com$', local_uid):
                local_uid = local_uid + '@google.com'
            if (local_uid == uid):
                if e.extended_property:
                    for num, p in enumerate(e.extended_property):
                        if (p.name == 'local_uid'):
                            return p.value
        return None

    # Retrieve an event from Google Calendar by local UID.
    def get_event_by_uid(self, uid):
        for e in self.elements():
            if e.extended_property:
                for num, p in enumerate(e.extended_property):
                    if (p.name == 'local_uid'):
                        if (p.value == uid):
                            return e
        return None

    # Insert a new Google Calendar event.
    def insert(self, event):
        try:
            e = calendar.CalendarEventEntry()
        except:
            print >> sys.stderr, 'ERROR: couldn\'t create gdata calendar object'
            sys.exit(1)
        self.ical2gcal(e, event)
        try:
            # Get calendar private feed URL.
            new_event = self.calendar_service.InsertEvent(
                e, '/calendar/feeds/default/private/full')
            print 'New event inserted (%s): %s' % (self.private_url, new_event.id.text,)
        except Exception, e:
            print >> sys.stderr, 'WARNING: couldn\'t insert entry %s to %s!' \
                % (event.uid.value, self.private_url)
            print type(e), e.args, e
        return new_event

    # Update a Google Calendar event.
    def update(self, event):
        e = self.get_event_by_uid(event.uid.value)
        if e is None:
            print >> sys.stderr, 'WARNING: event %s not found in %s' % (
                event.uid.value, self.private_url)
            return
        self.ical2gcal(e, event)
        try:
            new_event = self.calendar_service.UpdateEvent(
                e.GetEditLink().href, e)
            print 'Updated event (%s): %s' % (self.private_url, new_event.id.text,)
        except Exception, e:
            print >> sys.stderr, 'WARNING: couldn\'t update entry %s to %s!' % (
                event.uid.value, self.private_url)
            print type(e), e.args, e
        return new_event

    # Delete a Google Calendar event.
    def delete(self, event):
        e = self.get_event_by_uid(event.uid.value)
        if e is None:
            print >> sys.stderr, 'WARNING: event %s not found in %s!' % (
                event.uid.value, self.private_url)
            return
        try:
            self.calendar_service.DeleteEvent(e.GetEditLink().href)
            print 'Deleted event (%s): %s' % (self.private_url, e.id.text,)
        except Exception, e:
            print >> sys.stderr, 'WARNING: couldn\'t delete entry %s in %s!' % (
                event.uid.value, self.private_url)
            print type(e), e.args, e

    # List all the Google Calendar events.
    def list(self):
        for e in self.elements():
            print e.title.text, '-->', e.id.text

    # Commit changes to Google Calendar.
    def sync(self):
        print 'Synchronized ', self.private_url
        pass


class iCalCalendar:

    def __init__(self, url, login=None, password=None):
        self.url = url
        try:
        	# Local calendar.
            stream = file(self.url)
            content = stream.read()
            stream.close()
            self.cal = vobject.readOne(content, findBegin='false')
        except:
            # Create an empty calendar object.
            self.cal = vobject.iCalendar()

    # Return the list of events in the iCal Calendar.
    def elements(self):
        ret = []
        for event in self.cal.components():
            if (event.name == 'VEVENT') and hasattr(event, 'summary') and hasattr(event, 'uid'):
                ret.append(event)
        return ret

    # Retrieve an event from Google Calendar by local UID.
    def get_event_by_uid(self, uid):
        for e in self.elements():
            if e.uid.value == uid:
                return e
        return None

    # Insert a new iCal event.
    def insert(self, event):
        self.cal.add(event)
        print 'New event inserted (%s): %s' % (self.url, event.uid.value)
        return event

    # Update a Google Calendar event.
    def update(self, event):
        e = self.get_event_by_uid(event.uid.value)
        if e is None:
            print >> sys.stderr, 'WARNING: event %s not found in %s!' % (
                event.uid.value, self.url)
            return
        e.copy(event)
        print 'Updated event (%s): %s' % (self.url, e.uid.value,)
        return event

    # Delete a iCal Calendar event.
    def delete(self, event):
        e = self.get_event_by_uid(event.uid.value)
        self.cal.remove(e)
        print 'Deleted event (%s): %s' % (self.url, e.uid.value,)

    # List all the iCal events.
    def list(self):
        for event in self.elements():
            print event.summary.value, '-->', event.uid.value

    # Commit changes to iCal Calendar.
    def sync(self):
        print 'Synchronized ', self.url
        m = re.match('^http', self.url)
        if m:
            print >> sys.stderr, 'ERROR: couldn\'t sync a remote calendar directly: ', self.url
            sys.exit(1)
        try:
            f = open(self.url, 'w')
            f.write(unicode(self.cal.serialize()).encode('ascii', 'replace'))
            f.close()
        except Exception, e:
            print >> sys.stderr, 'ERROR: couldn\'t write to local calendar: ', self.url
            print type(e), e.args, e
            sys.exit(1)


# Class used for logging stuff.
class Logger:
    def __init__(self, f):
        self.f = f

    def write(self, s):
        self.f.write(s)
        self.f.flush()


def main():

    usage = '''\
	migration_calendar.py --user=USER_NAME --file=ICS_FILE
	'''

    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-u', '--user', help='nombre de usuario a migrar')
    parser.add_option('-f', '--file', help='archivo -ics')
    options, args = parser.parse_args()

    if args:
        parser.print_help()
        parser.exit(msg='\nUnexpected arguments: %s' % ' '.join(args))

    user_name = str(options.user)
    ics_file = options.file

    if not user_name:
        print 'Debes ingresar el nombre de usuario a migrar'

    if not ics_file:
        print 'Debes ingresar un archivo .ics'

    oauth_manager = OAuthManager(user_name, CONSUMER_KEY, CONSUMER_SECRET)

    calendar_manager = CalendarManager(
        user_name, CONSUMER_KEY, oauth_manager.two_legged_oauth_token)

    # leyendo el archivo ics
    ical = iCalCalendar(ics_file)

    for event in ical.elements():
    	#calendar_manager.insert(event)


if __name__ == '__main__':
    main()
